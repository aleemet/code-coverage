import org.junit.Test;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

public class ParkingLotTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    private final List<Car> parkingLot = new ArrayList<Car>(Arrays.asList(
            new Car("459MMM", "Maarja Maa", 2.25),
            new Car("987TMT", "Ants Asi", 2.55),
            new Car("777OOO", "Maarja Maa", 2.15)));

    @Test
    public void countCarsTest() throws IOException {
        Queue<Car> cars = ParkingLot.countCars("parkingLotCars.dat");

        for (Car car : parkingLot){
            Car comparable = cars.poll();
            assertEquals(car.getOwner(), comparable.getOwner());
            assertEquals(car.getRegistrationNumber(), comparable.getRegistrationNumber());
            assertEquals(car.getHeight(), comparable.getHeight(), 0.001);
        }
    }

    @Test
    public void carsOfOwnersTest() {
        Map<String, List<Car>> carOwners = ParkingLot.carsOfOwner(parkingLot);
        for (Car car : parkingLot){
            List<Car> cars = carOwners.get(car.getOwner());
            assertTrue(cars.contains(car));
        }
    }

    @Test
    public void savedCarsTest() throws IOException {
        ParkingLot.savedCars(parkingLot);
        try (BufferedReader br = new BufferedReader(new FileReader("parkingLot.txt"))){
            for (Car car : parkingLot){
                String line = br.readLine();
                assertEquals(line, car.toString());
            }
        }
    }

    @Test
    public void viewCarsTest() throws IOException {
        captureOut();
        ParkingLot.viewCars(parkingLot, new BufferedReader( new InputStreamReader(new ByteArrayInputStream("Valve Varblane\nMaarja Maa".getBytes()))));
        String output = getOut();
        assertEquals("The following people are car owners: [Ants Asi, Maarja Maa]\n" +
                        "Enter the car owner name: Owner does not exist. Try again: [Ants Asi, Maarja Maa]\n" +
                        "Enter the car owner name: Car: 459MMM Maarja Maa 2.25\n" +
                        "Car: 459MMM Maarja Maa 2.25\n" +
                        "Car: 777OOO Maarja Maa 2.15\n",
                output);
    }

    @Test
    public void addToLotTest() throws IOException {
        Queue<Car> cars = ParkingLot.countCars("parkingLotCars.dat");
        cars.poll();
        captureOut();
        ParkingLot.addToLot(cars, parkingLot);
        ParkingLot.addToLot(cars, parkingLot);
        String output = getOut();
        assertEquals("Car: 987TMT Ants Asi 2.55\n" +
                        "The car is too big! Not allowed to enter!\n" +
                        "Car: 777OOO Maarja Maa 2.15\n" +
                        "Car is in parking lot. Parking started at:",
                output.substring(0, output.length()-25));
        captureOut();
    }

    @Test
    public void addToLotTest2() throws IOException {
        Queue<Car> cars = new LinkedList<>();
        cars.poll();
        captureOut();
        ParkingLot.addToLot(cars, parkingLot);
        String output = getOut();
        assertEquals("There are no cars in the queue.\n",
                output);
        captureOut();
    }

    @Test
    public void mainTest() throws IOException {

        String expected = "Would you like to (L)eave your car to the parking lot, (V)iew the cars or (S)ave and exit?Unknown command. Try again.\n" +
                "Would you like to (L)eave your car to the parking lot, (V)iew the cars or (S)ave and exit?Car: 459MMM Maarja Maa 2.25\n" +
                "Car is in parking lot. Parking started at: \n" +
                "Would you like to (L)eave your car to the parking lot, (V)iew the cars or (S)ave and exit?Car: 987TMT Ants Asi 2.55\n" +
                "The car is too big! Not allowed to enter!\n" +
                "Would you like to (L)eave your car to the parking lot, (V)iew the cars or (S)ave and exit?Car: 777OOO Maarja Maa 2.15\n" +
                "Car is in parking lot. Parking started at: \n" +
                "Would you like to (L)eave your car to the parking lot, (V)iew the cars or (S)ave and exit?Car: 123ABC Mati Kaalupomm 2.05\n" +
                "Car is in parking lot. Parking started at: \n" +
                "Would you like to (L)eave your car to the parking lot, (V)iew the cars or (S)ave and exit?Car: 365RNA Valve Varblane 2.75\n" +
                "The car is too big! Not allowed to enter!\n" +
                "Would you like to (L)eave your car to the parking lot, (V)iew the cars or (S)ave and exit?There are no cars in the queue.\n" +
                "Would you like to (L)eave your car to the parking lot, (V)iew the cars or (S)ave and exit?The following people are car owners: [Maarja Maa, Mati Kaalupomm]\n" +
                "Enter the car owner name: Car: 459MMM Maarja Maa 2.25 \n" +
                "Car: 459MMM Maarja Maa 2.25 \n" +
                "Car: 777OOO Maarja Maa 2.15 \n" +
                "Would you like to (L)eave your car to the parking lot, (V)iew the cars or (S)ave and exit?Cars saved!\n";
        Pattern timestamp = Pattern.compile("\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{3}");
        Matcher matcher = timestamp.matcher(expected);

        System.setIn(new ByteArrayInputStream("A\nL\nL\nL\nL\nL\nL\nV\nMaarja Maa\nS".getBytes()));
        captureOut();
        ParkingLot.main(new String[]{});

        String output = getOut();
        Matcher matcher2 = timestamp.matcher(output);

        assertEquals(matcher.replaceAll(""), matcher2.replaceAll(""));
    }

    private void captureOut() {
        System.setOut( new PrintStream( outContent ) );
    }

    private String getOut() {
        System.setOut( new PrintStream( new FileOutputStream( FileDescriptor.out ) ) );
        return outContent.toString().replaceAll( "\r", "" );

    }


}