

import java.io.*;
import java.util.*;

public class ParkingLot {

    public static Queue<Car> countCars(String file) throws IOException {

        Queue<Car> queue = new LinkedList<Car>();
        try (DataInputStream dis = new DataInputStream(new FileInputStream(file))) {
            for (int i = dis.readInt(); i > 0; i--) {
                queue.add(new Car(dis.readUTF(), dis.readUTF(), dis.readDouble()));
            }
        }

        return queue;
    }

    public static Map<String, List<Car>> carsOfOwner(List<Car> cars) {
        Map<String, List<Car>> carOwners = new HashMap<>();
        for (Car car : cars) {
            if (!carOwners.containsKey(car.getOwner()))
                carOwners.put(car.getOwner(), new ArrayList<>(Collections.singletonList(car)));
            carOwners.get(car.getOwner()).add(car);
        }
        return carOwners;
    }

    public static void savedCars(List<Car> parkingLot) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("parkingLot.txt"), "UTF-8"))) {
            for (Car car : parkingLot)
                writer.write(car.toString() + "\n");
        }
    }

    public static void main(String[] args) throws IOException {
        Queue<Car> cars = countCars("parkingLotCars.dat");
        List<Car> parkingLot = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            System.out.print("Would you like to (L)eave your car to the parking lot, (V)iew the cars or (S)ave and exit?");

            String line = reader.readLine();
            switch (line) {
                case "L":
                    addToLot(cars, parkingLot);
                    break;
                case "V":
                    viewCars(parkingLot, reader);
                    break;
                case "S":
                    savedCars(parkingLot);
                    System.out.println("Cars saved!");
                    reader.close();
                    return;
                default:
                    System.out.println("Unknown command. Try again.");

            }
        }
    }

    public static void viewCars(List<Car> parkingLot, BufferedReader reader) throws IOException {
        Map<String, List<Car>> carOwners = carsOfOwner(parkingLot);
        System.out.println("The following people are car owners: " + carOwners.keySet().toString());

        System.out.print("Enter the car owner name: ");
        String owner = reader.readLine();
        while (!carOwners.containsKey(owner)) {
            System.out.println("Owner does not exist. Try again: " + carOwners.keySet().toString());
            System.out.print("Enter the car owner name: ");
            owner = reader.readLine();
        }
        for (Car car : carOwners.get(owner))
            System.out.println(car);
    }

    public static void addToLot(Queue<Car> cars, List<Car> parkingLot) {
        try {
            Car car = cars.poll();
            System.out.println(car.toString());
            car.enterParkingLot(2.5);
            parkingLot.add(car);
            System.out.println("Car is in parking lot. Parking started at: " + car.getParkingStart());
        } catch (CarSizeException e) {
            System.out.println(e.getMessage());
        } catch (NullPointerException e) {
            System.out.println("There are no cars in the queue.");
        }
    }
}
