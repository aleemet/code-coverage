

public class CarSizeException extends RuntimeException {
    public CarSizeException(String message) {
        super(message);
    }
}
