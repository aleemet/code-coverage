

import java.time.LocalDateTime;

public class Car {
    private String registrationNumber;
    private String owner;
    private double height;
    private LocalDateTime parkingStart;

    public Car(String registrationNumber, String owner, double height) {
        this.registrationNumber = registrationNumber;
        this.owner = owner;
        this.height = height;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public String getOwner() {
        return owner;
    }

    public double getHeight() {
        return height;
    }

    public LocalDateTime getParkingStart() {
        return parkingStart;
    }

    public void enterParkingLot(double maxHeight){
        if (height <= maxHeight)
            parkingStart = LocalDateTime.now();
        else
            throw new CarSizeException("The car is too big! Not allowed to enter!");
    }

    @Override
    public String toString() {
        String end = "";
        if (parkingStart != null)
            end = " " + parkingStart;
        return "Car: " + registrationNumber + " " + owner + " " + height + end;
    }
}
